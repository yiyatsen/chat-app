Install.

``` bash
npm install
```

Run the socket server.

``` bash
npm run server
```

Run dev server, clients can be requested on localhost:3000

``` bash
npm run client
```