import React, { Component } from 'react';
import NavBar from './components/NavBar';
import './App.css';
import ConversationList from './components/ConversationList';
import LoginDialog from './components/LoginDialog';
// import Dialog from '@material-ui/core/Dialog';
// import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, TextField, Button } from '@material-ui/core';
//import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const io = require('socket.io-client')
const socket = io.connect('http://localhost:3001')
const chatroom_title = "Socket.io聊天室"

class App extends Component {
  constructor() {
    super();

    // this.socketAddUser = this.socketAddUser.bind(this);
  };

  state = {
    user: '',
    message: '',
    open: true,
    dense: true,
    messages: [
      // { text: '1', id: 0 },
      // { text: '2', id: 1 }
    ]
  };

  componentDidMount() {
    socket.on('chat message', (data) => {
      const state = this.state;
      const msg = "[" + data.username + "]發言:" + data.msg;
      state.messages.push({ text: msg, id: this.state.messages.length });
      state.message = "";
      this.setState(state);
    });

    socket.on('add user', (data) => {
      const msg = "[" + data.username + "] 已加入";
      const state = this.state;
      state.messages.push({ text: msg, id: this.state.messages.length });
      this.setState(state);
    });

    socket.on('user left', (data) => {
      const msg = "[" + data.username + "] 已離開";
      const state = this.state;
      state.messages.push({ text: msg, id: this.state.messages.length });
      this.setState(state);
    });
  }

  handleLoginClose = (user) => {
    socket.emit("add user", user);
    this.setState({ user });
  };

  handleKeyDown = (e) => {
    if (e.keyCode === 13) {
      this.handleSendMessageClick();
    }
  }

  handleSendMessageClick = () => {
    socket.emit('chat message', this.state.message);
  };

  render() {
    return (
      <div>
        <NavBar
          chatroom_title={chatroom_title}
          user={this.state.user}
        />

        <ConversationList
          dense={this.state.dense}
          messages={this.state.messages}
        />

        <TextField
          id="message-input"
          type="text"
          value={this.state.message}
          onKeyDown={this.handleKeyDown}
          onChange={e => this.setState({ message: e.target.value })}
        />

        <Button
          className="message-input"
          id="send"
          variant="contained"
          color="primary"
          onClick={this.handleSendMessageClick}
        >
          發言
        </Button>

        <LoginDialog
          open={this.state.open}
          chatroom_title={chatroom_title}
          onLoginClose={this.handleLoginClose}
        />
      </div>
    );
  }
}

export default App;
