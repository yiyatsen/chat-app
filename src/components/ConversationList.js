import React, { Component } from "react";
// import ReactDOM from "react-dom";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

class ConversationList extends Component {
  state = {
    dense: this.props.dense,
    messages: this.props.messages
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ messages: nextProps.messages })
  }

  render() {
    return (
      <List id="message_block" dense={this.state.dense}>
        {this.state.messages.map(dialog => (
          <ListItem key={dialog.id}>
            <ListItemText primary={dialog.text} />
          </ListItem>
        ))}
      </List>
    );
  }
}

export default ConversationList;