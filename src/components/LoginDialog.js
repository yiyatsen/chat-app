import React, { Component } from "react";
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, TextField, Button }
  from '@material-ui/core';

class LoginDialog extends Component {
  state = {
    chatroom_title: this.props.chatroom_title,
    name: 'guest',
    open: this.props.open
  };

  handleClose = (name) => {
    this.setState({ open: false });
    this.props.onLoginClose(name);
  };

  render() {
    return (
      <Dialog
        open={this.state.open}
        // onClose={this.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{this.state.chatroom_title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            輸入你的名稱
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            // label="你的名稱"
            fullWidth
            value={this.state.name}
            onChange={e => this.setState({ name: e.target.value })}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => this.handleClose(this.state.name)}
            color="primary">
            進入聊天室
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default LoginDialog;