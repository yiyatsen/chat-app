import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

const renderUserName = (user) => {
  if (user.length > 0) {
    return (" - [" + user + "]");
  }
  return "";
}

const NavBar = (props) => {
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="title" color="inherit">
            {props.chatroom_title}{renderUserName(props.user)}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default NavBar;